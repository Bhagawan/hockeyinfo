package com.example.hockeyapp.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.hockeyapp.R;
import com.example.hockeyapp.adapter.StandingsAdapter;
import com.example.hockeyapp.data.Contants;
import com.example.hockeyapp.data.StandingsPosition;
import com.example.hockeyapp.presenter.StandingsPresenter;
import com.example.hockeyapp.presenter.StandingsPresenterViewInterface;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class StandingsFragment extends MvpAppCompatFragment implements StandingsPresenterViewInterface {
    private View mView;

    @InjectPresenter
    StandingsPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_standings, container, false);

        SharedPreferences sharedpreferences = getContext().getSharedPreferences(Contants.MY_SAVED_SETTINGS, Context.MODE_PRIVATE);
        TabLayout tabLayout = mView.findViewById(R.id.tabs_standings);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                sharedpreferences.edit().putInt("currentTab",tab.getPosition()).apply();
                switch (tab.getPosition()) {
                    case 0:
                        mPresenter.showKHL();
                        break;
                    case 1:
                        mPresenter.showNHL();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        int n = sharedpreferences.getInt("currentTab",-1);
        if( n != -1) {
            switch (n) {
                case 0:
                    mPresenter.showKHL();
                    break;
                case 1:
                    mPresenter.showNHL();
                    break;
            }
            tabLayout.getTabAt(n).select();
        } else mPresenter.showKHL();
        return mView;
    }

    @Override
    public void showStandings(List<StandingsPosition> data) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_standings);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new StandingsAdapter(data));
    }
}