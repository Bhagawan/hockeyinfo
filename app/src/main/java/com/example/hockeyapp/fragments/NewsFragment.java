package com.example.hockeyapp.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hockeyapp.R;
import com.example.hockeyapp.adapter.NewsAdapter;
import com.example.hockeyapp.data.NewsPiece;
import com.example.hockeyapp.presenter.NewsPresenter;
import com.example.hockeyapp.presenter.NewsPresenterViewInterface;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;

public class NewsFragment extends MvpAppCompatFragment implements NewsPresenterViewInterface {
    private View view;

    @InjectPresenter
    NewsPresenter newsPresenter;

    @ProvidePresenter


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, container, false);

        return view;
    }

    @Override
    public void showNews(List<NewsPiece> data) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_news);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private boolean forward = true;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                forward = dx > 0;
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int i;
                    if (forward) i = linearLayoutManager.findLastVisibleItemPosition();
                    else i = linearLayoutManager.findFirstVisibleItemPosition();
                    linearLayoutManager.smoothScrollToPosition(recyclerView, null, i);
                    if(i != RecyclerView.NO_POSITION ) showArticle(data.get(i));
                }
            }
        });
        NewsAdapter adapter = new NewsAdapter(data);
        recyclerView.setAdapter(adapter);
        if(data != null) showArticle(data.get(0));
    }

    @Override
    public void onStop() {
        super.onStop();
        getMvpDelegate().onSaveInstanceState();
    }

    private void showArticle(NewsPiece article) {
        TextView header = view.findViewById(R.id.text_news_header);
        header.setText(article.getHeader());

        TextView text = view.findViewById(R.id.text_news_article);
        text.setText(article.getText());
    }

}