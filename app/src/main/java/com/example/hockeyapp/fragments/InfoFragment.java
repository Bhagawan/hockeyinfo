package com.example.hockeyapp.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.hockeyapp.R;
import com.example.hockeyapp.presenter.InfoPresenter;
import com.example.hockeyapp.presenter.InfoPresenterViewInterface;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class InfoFragment extends MvpAppCompatFragment implements InfoPresenterViewInterface {
    private View view;

    @InjectPresenter
    InfoPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_info, container, false);

        Button button = view.findViewById(R.id.btn_info_input);
        EditText editText = view.findViewById(R.id.editText_query_input);
        button.setOnClickListener(v -> mPresenter.sendQuery(editText.getText().toString()));
        return view;
    }

    @Override
    public void showAnswer(String text) {
        TextView output = view.findViewById(R.id.text_info_output);
        output.setText(text);
        View scrollView = view.findViewById(R.id.info_output_list);
        scrollView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        TextView output = view.findViewById(R.id.text_info_output);
        output.setText(getResources().getString(R.string.msg_info_not_found));
        View scrollView = view.findViewById(R.id.info_output_list);
        scrollView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getMvpDelegate().onSaveInstanceState();
    }
}