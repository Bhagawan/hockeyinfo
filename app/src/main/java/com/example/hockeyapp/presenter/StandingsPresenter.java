package com.example.hockeyapp.presenter;

import androidx.annotation.NonNull;

import com.example.hockeyapp.data.CashedList;
import com.example.hockeyapp.data.StandingsPosition;
import com.example.hockeyapp.util.MyServerClient;
import com.example.hockeyapp.util.ServerClient;

import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class StandingsPresenter extends MvpPresenter<StandingsPresenterViewInterface> {

    public void showNHL() {
        if(CashedList.getmNHLStandings() == null)  downloadNHL();
        else getViewState().showStandings(CashedList.getmNHLStandings());
    }

    public void showKHL() {
        if(CashedList.getmKHLStandings() == null)  downloadKHL();
        else getViewState().showStandings(CashedList.getmKHLStandings());
    }

    private void downloadNHL() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<List<StandingsPosition>> call = client.getNHLStandings();
        call.enqueue(new Callback<List<StandingsPosition>>() {
            @Override
            public void onResponse(@NonNull Call<List<StandingsPosition>> call, @NonNull Response<List<StandingsPosition>> response) {
                CashedList.setmNHLStandings(response.body());
                getViewState().showStandings(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<StandingsPosition>> call, @NonNull Throwable t) {

            }
        });
    }

    private void downloadKHL() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<List<StandingsPosition>> call = client.getKHLStandings();
        call.enqueue(new Callback<List<StandingsPosition>>() {
            @Override
            public void onResponse(@NonNull Call<List<StandingsPosition>> call, @NonNull Response<List<StandingsPosition>> response) {
                CashedList.setmKHLStandings(response.body());
                getViewState().showStandings(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<StandingsPosition>> call, @NonNull Throwable t) {

            }
        });
    }
}
