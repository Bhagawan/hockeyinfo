package com.example.hockeyapp.presenter;


import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;
import moxy.viewstate.strategy.alias.AddToEndSingle;

public interface InfoPresenterViewInterface extends MvpView {

    @AddToEnd
    void showAnswer(String text);

    @AddToEndSingle
    void showError();
}
