package com.example.hockeyapp.presenter;

import com.example.hockeyapp.data.NewsPiece;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface NewsPresenterViewInterface extends MvpView {

    @AddToEnd
    void showNews(List<NewsPiece> data);
}
