package com.example.hockeyapp.presenter;

import androidx.annotation.NonNull;

import com.example.hockeyapp.data.CashedList;
import com.example.hockeyapp.util.MyWikiClient;
import com.example.hockeyapp.util.WikiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class InfoPresenter extends MvpPresenter<InfoPresenterViewInterface> {

    @Override
    protected void onFirstViewAttach() {
        String a = CashedList.getAnswer();
        if(!a.isEmpty()) getViewState().showAnswer(a);
    }

    public void sendQuery(String text) {
        WikiClient wikiClient = MyWikiClient.createService(WikiClient.class);
        String query = text + " хоккейная команда";
        Call<String> call = wikiClient.wikiQuery(query);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                formatAndShowAnswer(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

            }
        });
    }

    private void formatAndShowAnswer(String body) {
        try {
            JSONObject object = new JSONObject(body);
            JSONObject query = object.getJSONObject("query");
            JSONObject page = query.getJSONObject("pages");
            JSONArray pages = page.names();
            String name;
            if(pages != null) {
                name = pages.getString(0);
                JSONObject result = page.getJSONObject(name);
                String t = result.getString("extract");
                CashedList.setmAnswer(t);
                getViewState().showAnswer(t);
            }
        } catch (JSONException e) {
            getViewState().showError();
            e.printStackTrace();
        }
    }
}
