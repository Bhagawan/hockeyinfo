package com.example.hockeyapp.presenter;

import com.example.hockeyapp.data.StandingsPosition;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface StandingsPresenterViewInterface extends MvpView {

    @AddToEnd
    void showStandings(List<StandingsPosition> data);
}
