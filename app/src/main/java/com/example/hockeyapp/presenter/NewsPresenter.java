package com.example.hockeyapp.presenter;

import androidx.annotation.NonNull;

import com.example.hockeyapp.data.NewsPiece;
import com.example.hockeyapp.util.MyServerClient;
import com.example.hockeyapp.util.ServerClient;

import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class NewsPresenter extends MvpPresenter<NewsPresenterViewInterface> {

    @Override
    protected void onFirstViewAttach() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<List<NewsPiece>> call = client.getNews();
        call.enqueue(new Callback<List<NewsPiece>>() {
            @Override
            public void onResponse(@NonNull Call<List<NewsPiece>> call, @NonNull Response<List<NewsPiece>> response) {
                getViewState().showNews(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<NewsPiece>> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
