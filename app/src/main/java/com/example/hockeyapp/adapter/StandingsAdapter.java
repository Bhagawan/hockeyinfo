package com.example.hockeyapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hockeyapp.R;
import com.example.hockeyapp.data.StandingsPosition;
import com.squareup.picasso.Picasso;

import java.util.List;

public class StandingsAdapter extends RecyclerView.Adapter<StandingsAdapter.ViewHolder> {
    private List<StandingsPosition> mStandings;

    public StandingsAdapter(List<StandingsPosition> data) { this.mStandings = data; }

    @NonNull
    @Override
    public StandingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_standings, parent, false);
        return new StandingsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StandingsAdapter.ViewHolder holder, int position) {
        StandingsPosition standingsPosition = mStandings.get(position);
        holder.num.setText(String.valueOf(standingsPosition.getNum()));
        Picasso.get().load(standingsPosition.getImg()).resize(45, 45).into(holder.logo);
        holder.name.setText(standingsPosition.getName());
        holder.games.setText(String.valueOf(standingsPosition.getGames()));
        holder.win.setText(String.valueOf(standingsPosition.getWin_main() + standingsPosition.getWin_overtime()));
        holder.loss.setText(String.valueOf(standingsPosition.getLoss_main() + standingsPosition.getLoss_overtime()));
        holder.goals.setText(standingsPosition.getGoals());
        holder.points.setText(String.valueOf(standingsPosition.getPoints()));
    }

    @Override
    public int getItemCount() {
       if(mStandings != null) return mStandings.size();
       else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView num;
        private ImageView logo;
        private TextView name;
        private TextView games;
        private TextView win;
        private TextView loss;
        private TextView goals;
        private TextView points;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            num = itemView.findViewById(R.id.text_standings_item_num);
            logo = itemView.findViewById(R.id.image_standings_item_logo);
            name = itemView.findViewById(R.id.text_standings_item_name);
            games = itemView.findViewById(R.id.text_standings_item_games);
            win = itemView.findViewById(R.id.text_standings_item_win);
            loss = itemView.findViewById(R.id.text_standings_item_lost);
            goals = itemView.findViewById(R.id.text_standings_item_goal);
            points = itemView.findViewById(R.id.text_standings_item_points);
        }
    }
}
