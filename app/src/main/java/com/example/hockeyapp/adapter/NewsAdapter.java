package com.example.hockeyapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hockeyapp.R;
import com.example.hockeyapp.data.NewsPiece;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private List<NewsPiece> news;
    private int mWidth, mHeight;

    public NewsAdapter(List<NewsPiece> news) { this.news = news; }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mWidth = recyclerView.getWidth();
        mHeight = recyclerView.getHeight();
    }

    @NonNull
    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
        return new NewsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.ViewHolder holder, int position) {
        Picasso.get().load(news.get(position).getImg()).resize(mWidth, mHeight).centerCrop().into(holder.newsImage);
        if(position == 0) holder.arrowLeft.setVisibility(View.GONE);
        if(position == news.size() - 1) holder.arrowRight.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        if(news != null) return news.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView arrowLeft;
        private ImageView arrowRight;
        private ImageView newsImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            arrowLeft = itemView.findViewById(R.id.image_news_item_left);
            arrowRight = itemView.findViewById(R.id.image_news_item_right);
            newsImage = itemView.findViewById(R.id.image_news_item);
        }
    }
}
