package com.example.hockeyapp.data;

import com.google.gson.annotations.SerializedName;

public class NewsPiece {
    @SerializedName("header")
    private String header;
    @SerializedName("img")
    private String img;
    @SerializedName("text")
    private String text;

    public String getHeader() { return header; }

    public String getImg() {
        return img;
    }

    public String getText() {
        return text;
    }
}
