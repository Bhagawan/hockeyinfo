package com.example.hockeyapp.data;

import com.google.gson.annotations.SerializedName;

public class StandingsPosition {
    @SerializedName("num")
    private int num;
    @SerializedName("img")
    private String img;
    @SerializedName("name")
    private String name;
    @SerializedName("games")
    private int games;
    @SerializedName("win_main")
    private int win_main;
    @SerializedName("win_overtime")
    private int win_overtime;
    @SerializedName("loss_main")
    private int loss_main;
    @SerializedName("loss_overtime")
    private int loss_overtime;
    @SerializedName("goals")
    private String goals;
    @SerializedName("points")
    private int points;

    public int getNum() {
        return num;
    }

    public String getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public int getGames() {
        return games;
    }

    public int getWin_main() {
        return win_main;
    }

    public int getWin_overtime() {
        return win_overtime;
    }

    public int getLoss_main() {
        return loss_main;
    }

    public int getLoss_overtime() {
        return loss_overtime;
    }

    public String getGoals() {
        return goals;
    }

    public int getPoints() {
        return points;
    }
}
