package com.example.hockeyapp.data;

import java.util.List;

public class CashedList {
    private static List<StandingsPosition> mNHLStandings;
    private static List<StandingsPosition> mKHLStandings;
    private static String mAnswer = "";

    public static List<StandingsPosition> getmNHLStandings() {
        return mNHLStandings;
    }

    public static void setmNHLStandings(List<StandingsPosition> mNHLStandings) {
        CashedList.mNHLStandings = mNHLStandings;
    }

    public static List<StandingsPosition> getmKHLStandings() {
        return mKHLStandings;
    }

    public static void setmKHLStandings(List<StandingsPosition> mKHLStandings) {
        CashedList.mKHLStandings = mKHLStandings;
    }

    public static String getAnswer() {
        return mAnswer;
    }

    public static void setmAnswer(String mAnswer) {
        CashedList.mAnswer = mAnswer;
    }
}
