package com.example.hockeyapp;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.hockeyapp.fragments.InfoFragment;
import com.example.hockeyapp.fragments.NewsFragment;
import com.example.hockeyapp.fragments.StandingsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import moxy.MvpAppCompatActivity;

public class MainActivity extends MvpAppCompatActivity {
    private Target target;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fullscreen();
        loadBackground();
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        BottomNavigationView menu  = findViewById(R.id.bottom_menu);

        getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainerView, new NewsFragment()).commit();
        menu.setOnApplyWindowInsetsListener(null);
        menu.setOnItemSelectedListener(item -> {
            if(item.getItemId() == R.id.news) {
                switchTo(new NewsFragment());
                return true;
            } else if(item.getItemId() == R.id.schedule) {
                switchTo(new InfoFragment());
                return true;
            } else if(item.getItemId() == R.id.standings) {
                switchTo(new StandingsFragment());
                return true;
            }
            return false;
        });
    }

    public void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void switchTo(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainerView, fragment).commit();
    }

    private void loadBackground() {
        ConstraintLayout c = findViewById(R.id.layout_base);
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                c.setBackground(new BitmapDrawable(getResources(),bitmap));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.get().load("http://159.69.89.54/HockeyApp/background.jpg").into(target);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        fullscreen();
        return super.dispatchTouchEvent( event );
    }
}