package com.example.hockeyapp.util;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WikiClient {

    @GET("w/api.php?format=json&action=query&prop=extracts&explaintext&origin=*&generator=search&gsrnamespace=0&gsrlimit=1")
    Call<String> wikiQuery(@Query("gsrsearch") String query);
}
