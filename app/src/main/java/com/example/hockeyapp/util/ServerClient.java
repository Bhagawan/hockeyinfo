package com.example.hockeyapp.util;

import com.example.hockeyapp.data.NewsPiece;
import com.example.hockeyapp.data.StandingsPosition;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ServerClient {

    @GET("HockeyApp/news.json")
    Call<List<NewsPiece>> getNews();

    @GET("HockeyApp/NHL.json")
    Call<List<StandingsPosition>> getNHLStandings();

    @GET("HockeyApp/KHL.json")
    Call<List<StandingsPosition>> getKHLStandings();

}
